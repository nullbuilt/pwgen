# pwgen - simple dictionary-based password generator

A simple password generator.  Just run it and it generates a 
password consisting of three randomly-picked English 
words, one capital letter, one number and one special character.

## Building

1. [Install Rust](https://rustup.rs/)
1. Clone this repositoy: `git clone https://gitlab.com/nullbuilt/pwgen.git`
1. `cd pwgen`
1. `cargo build --release`
1. Executable `pwgen` or `pwgen.exe` can be found in `target/release`

## Usage

```
pwgen 0.2.0
Andrew Dorton <nullbuiltsoft@gmail.com>
Simple dictionary-based password generator

USAGE:
    pwgen.exe [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -w, --wordlist <WORDLIST>    Custom wordlist file to use instead of default list

SUBCOMMANDS:
    help        Prints this message or the help of the given subcommand(s)
    wordlist    output default wordlist and exit
```

### -w --wordlist

```sh
$ pwgen -w words.txt
$ pwgen --wordlist=words.txt
```

Provide a custom wordlist to be used in place of the default wordlist.  Each word must be
on a separate line.  Each line must be delimited by a single newline `\n`.

### wordlist

```
pwgen.exe-wordlist
output default wordlist and exit

USAGE:
    pwgen.exe wordlist

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
```

```sh
$ pwgen wordlist
```

Sub-command that prints the default built-in wordlist and exits.  No password is generated in this mode.

## Disclaimer

I wrote this program as an excersise in learning Rust.  It has no tests 
and has only been built and tested on Windows 10.
