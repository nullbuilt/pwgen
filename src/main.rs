#[macro_use]
extern crate clap;
extern crate rand;

use rand::Rng;
use std::io::prelude::*;
use std::fs::File;

const MAX_WORDS: i32 = 3;

fn capitalize(s: &String) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}

fn rand_symbol() -> String {
    let symbols = "!@#$%^&*()_+=";
    let i = rand::thread_rng().gen_range(0, symbols.len());
    symbols.chars().nth(i).unwrap().to_string()
}

fn rand_number() -> String {
    let symbols = "0123456789";
    let i = rand::thread_rng().gen_range(0, symbols.len());
    symbols.chars().nth(i).unwrap().to_string()
}

fn main() {
    let matches = clap_app!(pwgen =>
        (version: crate_version!())
        (author: crate_authors!("\n"))
        (about: "Simple dictionary-based password generator")
        (@arg WORDLIST: -w --wordlist +takes_value "Custom wordlist file to use instead of default list")
        (@subcommand wordlist =>
            (about: "output default wordlist and exit")
        )
    ).get_matches();

    let raw_wordlist = if let Some(wordlist_file) = matches.value_of("WORDLIST") {
        let mut f = File::open(wordlist_file).expect(&format!("File '{}' not found", wordlist_file));
        let mut contents = String::new();
        f.read_to_string(&mut contents)
            .expect(&format!("error reading '{}'", wordlist_file));
        contents
    } else {
        include_str!("words.txt").into()
    };

    if let Some(_) = matches.subcommand_matches("wordlist") {
        print!("{}", raw_wordlist);
        std::process::exit(0);
    }

    let words: Vec<String> = raw_wordlist.split("\n").map(|s| s.to_string()).collect();
    let mut num_words = 0;
    let mut picked_words: Vec<String> = vec![];
    let capindex = rand::thread_rng().gen_range(0, MAX_WORDS);
    let symbol_index = rand::thread_rng().gen_range(0, MAX_WORDS);
    let number_index = rand::thread_rng().gen_range(0, MAX_WORDS);
    loop {
        if num_words >= MAX_WORDS {
            break;
        }
        let picked = rand::thread_rng().choose(&words).unwrap();
        if !picked_words.contains(picked) {
            if num_words == capindex {
                picked_words.push(capitalize(picked));
            } else {
                picked_words.push(picked.to_string());
            }
            if num_words == symbol_index {
                picked_words.push(rand_symbol());
            }
            if num_words == number_index {
                picked_words.push(rand_number());
            }
            num_words += 1;
        }
    }
    let pw = picked_words.join("");
    println!("{}", pw);
}
